@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Fiche : {{ $school->name }}</div>

                <div class="card-body">

                  <div class="card bg-light mb-3">
                    <div class="card-header font-weight-bold text-uppercase">{{ $school->name }}</div>
                  </div>


                    <a class="btn btn-success mb-2" href="{{ route('schools.index') }}">Retour a la liste</a>

                    <a class="btn btn-primary mb-2" href="{{ route('schools.edit', $school->id) }}">Modifier</a>

                    <form action="{{ route('schools.destroy') }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="hidden" name="id" value="{{ $school->id }}">
                        <button class="btn btn-danger" type="submit">Supprimer</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
