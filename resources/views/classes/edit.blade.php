@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Classes</div>

                <div class="card-body">
                    <a href="{{ route('classes.index') }}" class="btn btn-danger">Retour a la liste</a>

                    <br/>
                    <br/>

                    <form action="{{route('classes.update',$class->id)}}" method="post">
                        @csrf
                        @method('PUT')

                        <div class="form-group">
                            <label for="name">Nom</label>
                            @if(!is_null($class->name))
                            <input type="text" name="name" id="name" value="{{$class->name}}" class="form-control">
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="school_id">École</label>
                            <select name="school_id" id="school_id" class="form-control">
                                <option value="0"></option>
                                @foreach($schools as $school)
                                    <option value="{{$school->id}}" {{$class->school_id == $school->id ? 'selected':''}}>{{$school->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <button class="btn btn-success" type="submit" name="button">Enregister</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
