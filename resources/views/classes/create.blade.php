@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Classes</div>

                <div class="card-body">
                    <a href="{{ route('classes.index') }}" class="btn btn-danger">Retour a la liste</a>

                    <br/>
                    <br/>

                    <form action="{{route('classes.store')}}" method="post">
                        @csrf

                        <div class="form-group">
                            <label for="name">Nom</label>
                            <input type="text" name="name" id="name" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="school_id">École</label>
                            <select name="school_id" id="school_id" class="form-control">
                                <option value="0"></option>
                                @foreach($schools as $school)
                                    <option value="{{$school->id}}">{{$school->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <button class="btn btn-success" type="submit" name="button">Enregister</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
