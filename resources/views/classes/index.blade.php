@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Classes</div>

                <div class="card-body">
                    <a href="{{ route('classes.create') }}" class="btn btn-secondary">Nouvelle classe</a>
                    <br><br>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Nom</th>
                                <th scope="col">École</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($classes as $class)
                            <tr>
                                @if(!is_null($class->id) && !is_null($class->name))
                                <td><a href="{{route("classes.show",$class->id)}}">{{$class->name}}</a></td>
                                @else
                                <td></td>
                                @endif
                                @if(!is_null($class->school_id) && !is_null($class->school) && !is_null($class->school->name))
                                <td>{{$class->school->name}}</td>
                                @else
                                <td></td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
