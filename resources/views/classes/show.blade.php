@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Classes</div>

                <div class="card-body">
                    <a href="{{ route('classes.index') }}" class="btn btn-secondary">Retour a la liste</a>
                    <a href="{{ route('classes.edit',$class->id) }}" class="btn btn-info">Modifier cette classe</a>
                    <form action="{{ route('classes.destroy') }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="hidden" name="id" value="{{ $class->id }}">
                        <button class="btn btn-danger" type="submit">Supprimer cette classe</button>
                    </form>

                    <br><br>

                    <form>
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">Nom</label>
                            @if(!is_null($class->name))
                            <div class="col-sm-10">
                                <input id="name" readonly type="text" name="name" class="form-control-plaintext" value="{{$class->name}}">
                            </div>
                            @endif
                        </div>
                        <div class="form-group row">
                            <label for="school" class="col-sm-2 col-form-label">École</label>
                            @if(!is_null($class->school_id) && !is_null($class->school) && !is_null($class->school->name))
                            <div class="col-sm-10">
                                <input type="text" readonly name="school" id="school" class="form-control-plaintext" value="{{$class->school->name}}">
                            </div>
                            @endif
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
