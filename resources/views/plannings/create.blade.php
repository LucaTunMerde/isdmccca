@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Plannings</div>

                <div class="card-body">
                    <a href="{{ route('plannings.index') }}" class="btn btn-danger">Retour a la liste</a>

                    <br/>
                    <br/>

                    <form action="{{route('plannings.store')}}" method="post">
                        @csrf

                        <div class="form-group">
                            <label for="nom_cours">Nom du cours</label>
                            <input type="text" name="nom_cours" id="nom_cours" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="class_id">Classe</label>
                            <select name="class_id" id="class_id" class="form-control">
                                <option value="0"></option>
                                @foreach($classes as $class)
                                    <option value="{{$class->id}}">{{$class->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <button class="btn btn-success" type="submit" name="button">Enregister</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
