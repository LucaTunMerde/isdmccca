@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Plannings</div>

                <div class="card-body">
                    <a href="{{ route('plannings.show', $planning->id) }}" class="btn btn-danger">Retour au planning</a>

                    <br/>
                    <br/>

                    <form action="{{route('plannings.store_date', $planning->id)}}" method="post">
                        @csrf

                        <div class="form-group">
                            <label for="start_date">Date de debut</label>
                            <input type="date" name="start_date" id="start_date" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="start_hour">Heure de debut</label>
                            <input type="text" name="start_hour" id="start_hour" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="end_date">Date de fin</label>
                            <input type="date" name="end_date" id="end_date" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="end_hour">Heure de fin</label>
                            <input type="text" name="end_hour" id="end_hour" class="form-control">
                        </div>

                        <button class="btn btn-success" type="submit" name="button">Enregister</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
