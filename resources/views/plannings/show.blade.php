@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Plannings</div>

                <div class="card-body">
                    <a href="{{ route('plannings.index') }}" class="btn btn-secondary">Retour a la liste</a>
                    <a href="{{ route('plannings.edit',$planning->id) }}" class="btn btn-info">Modifier ce planning</a>
                    <a href="{{ route('plannings.date',$planning->id) }}" class="btn btn-info">Ajouter une date</a>
                    <form action="{{ route('plannings.destroy') }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="hidden" name="id" value="{{ $planning->id }}">
                        <button class="btn btn-danger" type="submit">Supprimer ce planning</button>
                    </form>

                    <br><br>

                    <form>
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">Nom du cours</label>
                            @if(!is_null($planning->nom_cours))
                            <div class="col-sm-10">
                                <input id="nom_cours" readonly type="text" name="nom_cours" class="form-control-plaintext" value="{{$planning->nom_cours}}">
                            </div>
                            @endif
                        </div>
                        <div class="form-group row">
                            <label for="class_id" class="col-sm-2 col-form-label">Classe</label>
                            @if(!is_null($planning->class_id) && !is_null($planning->class) && !is_null($planning->class->name))
                            <div class="col-sm-10">
                                <input type="text" readonly name="class_id" id="class_id" class="form-control-plaintext" value="{{$planning->class->name}}">
                            </div>
                            @endif
                        </div>
                    </form>

                    @foreach($planning->planning_dates as $date)
                        <p>{{$date->start_date}}</p>
                        <p>{{$date->end_date}}</p>
                        <a href="{{ route('plannings.edit_date', [$planning->id, $date->id] )}}">Modifier cette date</a>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
