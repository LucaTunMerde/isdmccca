@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Plannings</div>

                <div class="card-body">
                    <a href="{{ route('plannings.create') }}" class="btn btn-secondary">Nouveau planning</a>
                    <br><br>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Nom du cours</th>
                                <th scope="col">Classe</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($plannings as $planning)
                            <tr>
                                @if(!is_null($planning->id) && !is_null($planning->nom_cours))
                                <td><a href="{{route("plannings.show",$planning->id)}}">{{$planning->nom_cours}}</a></td>
                                @else
                                <td></td>
                                @endif
                                @if(!is_null($planning->class_id) && !is_null($planning->class) && !is_null($planning->class->name))
                                <td>{{$planning->class->name}}</td>
                                @else
                                <td></td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
