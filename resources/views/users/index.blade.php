@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Liste des utilisateurs</div>

                <div class="card-body">

                    <table class="table">

                      <thead class="thead-dark">
                        <tr>
                          <th scope="col">Nom</th>
                          <th scope="col">E-mail</th>
                          <th scope="col">Rôle</th>
                          <th scope="col">École</th>
                          <th scope="col">Classe</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($users as $user)
                        <tr>
                          <th scope="row"><a href="{{ route('users.show', $user->id) }}" title="{{ $user->name }}">{{ $user->name }}</a></th>

                          @if(!is_null($user->email))
                          <td>{{ $user->email }} €</td>
                          @else
                          <td></td>
                          @endif

                          @if(!is_null($user->role))
                          <td>{{ $user->role}}</td>
                          @else
                          <td></td>
                          @endif

                          @if(!is_null($user->school) && !is_null($user->school->name))
                          <td>{{ $user->school->name }}</td>
                          @else
                          <td></td>
                          @endif

                          @if(!is_null($user->class) && !is_null($user->class->name))
                          <td>{{ $user->class->name }}</td>
                          @else
                          <td></td>
                          @endif

                        </tr>
                        @endforeach
                      </tbody>

                    </table>

                    <a href="{{ route('home') }}" class="btn btn-secondary" title="Retour sur la page d'accueil">Retour sur la page d'accueil</a>

                    <a href="{{ route('users.create') }}" class="btn btn-success" title="Ajouter une user">Ajouter un utilisateur</a>

                    <br>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
