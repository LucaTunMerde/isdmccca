<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/* Schools routes */
Route::get('/schools', 'SchoolController@index')->name('schools.index');

Route::get('/schools/{id}/show', 'SchoolController@show')->name('schools.show');

Route::get('/schools/create', 'SchoolController@create')->name('schools.create');
Route::post('/schools', 'SchoolController@store')->name('schools.store');

Route::get('/schools/{id}/edit', 'SchoolController@edit')->name('schools.edit');
Route::put('/schools/{id}', 'SchoolController@update')->name('schools.update');

Route::delete('/schools', 'SchoolController@destroy')->name('schools.destroy');


/* Classes routes */
Route::get('/classes', 'ClassController@index')->name('classes.index');

Route::get('/classes/create', 'ClassController@create')->name('classes.create');
Route::post('/classes', 'ClassController@store')->name('classes.store');

Route::get('/classes/{id}/show', 'ClassController@show')->name('classes.show');

Route::get('/classes/{id}/edit', 'ClassController@edit')->name('classes.edit');
Route::put('/classes/{id}', 'ClassController@update')->name('classes.update');

Route::delete('/classes', 'ClassController@destroy')->name('classes.destroy');


/* Plannings routes */
Route::get('/plannings', 'PlanningController@index')->name('plannings.index');

Route::get('/plannings/create', 'PlanningController@create')->name('plannings.create');
Route::post('/plannings', 'PlanningController@store')->name('plannings.store');

Route::get('/plannings/{id}/show', 'PlanningController@show')->name('plannings.show');

Route::get('/plannings/{id}/edit', 'PlanningController@edit')->name('plannings.edit');
Route::put('/plannings/{id}', 'PlanningController@update')->name('plannings.update');

Route::delete('/plannings', 'PlanningController@destroy')->name('plannings.destroy');


/* Planning dates routes */
Route::get('/plannings/{id}/date/create', 'PlanningController@date')->name('plannings.date');
Route::post('/plannings/{id}/date/create', 'PlanningController@store_date')->name('plannings.store_date');

Route::get('/plannings/{id}/date/edit/{date_id}', 'PlanningController@edit_date')->name('plannings.edit_date');
Route::post('/plannings/{id}/date/edit/{date_id}', 'PlanningController@update_date')->name('plannings.store_date');


/* TESTING */
Route::get('/admin', 'AdminController@index')->name('admin')->middleware('admin');
Route::get('/no_access', 'HomeController@no_access')->name('no_access');


Route::get('/schools', 'SchoolController@index')->name('schools.index');
Route::get('/schools/{id}/show', 'SchoolController@show')->name('schools.show');
Route::get('/schools/create', 'SchoolController@create')->name('schools.create');
Route::get('/schools/{id}/edit', 'SchoolController@edit')->name('schools.edit');
Route::post('/schools', 'SchoolController@store')->name('schools.store');
Route::put('/schools/{id}', 'SchoolController@update')->name('schools.update');
Route::delete('/schools', 'SchoolController@destroy')->name('schools.destroy');

Route::get('/users', 'UserController@index')->name('users.index');
Route::get('/users/{id}/show', 'UserController@show')->name('users.show');
Route::get('/users/create', 'UserController@create')->name('users.create');
Route::get('/users/{id}/edit', 'UserController@edit')->name('users.edit');
Route::post('/users', 'UserController@store')->name('users.store');
Route::put('/users/{id}', 'UserController@update')->name('users.update');
Route::delete('/users', 'UserController@destroy')->name('users.destroy');
