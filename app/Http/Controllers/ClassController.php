<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Class_;
use App\Models\School;

class ClassController extends Controller
{
    public function index(){
        $classes = Class_::all();
        return view("classes.index", compact('classes'));
    }

    public function create(){
        $classes = Class_::all();
        $schools = School::all();

        return view("classes.create",compact('classes', 'schools'));
    }

    public function store(Request $request){
        $class = new Class_();

        $class->name = $request->get('name') != null ? $request->get('name') : 'undefined';
        $class->school_id = $request->get('school_id') != null ? $request->get('school_id') : 0;

        $class->save();

        return redirect()->route('classes.index');
    }

    public function show($id){
        $class = Class_::find($id);

        return view('classes.show',compact('class'));
    }

    public function edit($id){
        $class = Class_::find($id);
        $schools = School::all();


        return view('classes.edit',compact('class','schools'));
    }

    public function update(Request $request,$id){
        $class = Class_::find($id);
        $class->name = $request->get('name') != null ? $request->get('name') : $class->name;
        $class->school_id = $request->get('school_id') != null ? $request->get('school_id') : $class->school_id;
        $class->save();

        $schools = School::all();
        return view('classes.edit',compact('class','schools'));
    }

    public function destroy(Request $request){
        $class = Class_::find($request->get('id'));
        $class->delete();

        return redirect()->route('classes.index');
    }
}
