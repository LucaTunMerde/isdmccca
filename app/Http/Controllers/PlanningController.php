<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Planning;
use App\Models\Class_;
use App\Models\Planning_date;

class PlanningController extends Controller
{
    public function index(){
        $plannings = Planning::all();
        return view("plannings.index", compact('plannings'));
    }

    public function create(){
        $plannings = Planning::all();
        $classes = Class_::all();

        return view("plannings.create",compact('classes', 'classes'));
    }

    public function store(Request $request){
        $planning = new Planning();

        $planning->class_id = $request->get('class_id') != null ? $request->get('class_id') : 0;
        $planning->nom_cours = $request->get('nom_cours') != null ? $request->get('nom_cours') : 'undefined';

        $planning->save();

        return redirect()->route('plannings.index');
    }

    public function show($id){
        $planning = Planning::find($id);

        return view('plannings.show',compact('planning'));
    }

    public function edit($id){
        $planning= Planning::find($id);
        $classes = Class_::all();


        return view('plannings.edit',compact('planning','classes'));
    }

    public function update(Request $request,$id){
        $planning = Planning::find($id);
        $planning->class_id = $request->get('class_id') != null ? $request->get('class_id') : $planning->class_id;
        $planning->nom_cours = $request->get('nom_cours') != null ? $request->get('nom_cours') : $planning->nom_cours;
        $planning->save();

        $classes = Class_::all();
        return view('plannings.edit',compact('planning','classes'));
    }

    public function destroy(Request $request){
        $planning = Planning::find($request->get('id'));
        $planning->delete();

        return redirect()->route('plannings.index');
    }

    public function date($id){
        $planning = Planning::find($id);

        return view('plannings.date', compact('planning'));
    }

    public function store_date(Request $request, $id){
        $date = new Planning_date();

        $start_date = $request->get('start_date') . ' ' . $request->get('start_hour'). ':00';
        $end_date = $request->get('start_date') . ' ' . $request->get('end_hour') . ':00';


        $date->planning_id = $id;
        $date->start_date = $request->get('start_date') != null ? $start_date : '0000-01-01 00:00:00';
        $date->end_date = $request->get('end_date') != null ? $end_date : '0000-01-01 23:59:00';

        $date->save();

        return redirect()->route('plannings.show',$id);
    }

    public function edit_date($id, $date_id){
        $planning = Planning::find($id);
        $date = Planning_date::find($date_id);

        return view('plannings.date_edit', compact('date', 'planning'));
    }

    public function update_date(Request $request, $id, $date_id){
        $date = Planning_date::find($date_id);

        $start_date = $request->get('start_date') . ' ' . $request->get('start_hour'). ':00';
        $end_date = $request->get('start_date') . ' ' . $request->get('end_hour') . ':00';


        $date->planning_id = $id;
        $date->start_date = $request->get('start_date') != null ? $start_date : $date->start_date;
        $date->end_date = $request->get('end_date') != null ? $end_date : $date->end_date;

        $date->save();

        return redirect()->route('plannings.show',$id);
    }



}
