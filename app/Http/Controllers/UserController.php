<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Models\Class_;
use App\Models\School;

class UserController extends Controller
{

  // public function __construct()
  // {
  //   $this->middleware('auth');
  // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $users = User::all();
      $schools = School::all();
      $classes = Class_::all();
      return view('users.index', compact('users', 'schools', 'classes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $schools = School::all();
      $classes = Class_::all();
      return view('users.create', compact('schools', 'classes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $user = new User();
      $user->name = $request->get('name');
      $user->email = $request->get('email');
      $user->password = "";
      $user->school_id = $request->get('school_id');
      $user->class_id = $request->get('class_id');

      $user->save();

      return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $user = User::find($id);
      return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $users = User::all();
      $schools = School::all();
      $classes = Class_::all();

      return view('users.edit', compact('users', 'schools', 'classes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $user = User::find($id);
      $user->name = $request->get('name');
      $user->email = $request->get('email');
      $user->role = $request->get('role');
      $user->school_id = $request->get('school_id');
      $user->class_id = $request->get('class_id');

      $user->save();

      return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $user = User::find($request->get('id'));
      $user->delete();

      return redirect()->route('users.index');
    }
}
