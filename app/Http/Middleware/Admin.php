<?php

namespace App\Http\Middleware;

use Closure;
use Auth;


class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check()){
            return redirect()->route('login');
        }

        //If the logged user is not an admin redirect him to homepage
        if(Auth::user()->role != 0){
            return redirect()->route('no_access');
        }

        //If user is logged and is an admin redirect him to requested route
        return $next($request);

    }
}
