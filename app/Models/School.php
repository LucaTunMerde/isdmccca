<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Class_;

class School extends Model
{
    protected $table = 'schools';

    protected $fillable = [
        'id', 'name',
    ];

    public function classes(){
        return $this->hasMany(Class_::class);
    }
}
