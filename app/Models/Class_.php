<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\School;

class Class_ extends Model
{
    protected $table = 'classes';

    protected $fillable = [
        'id', 'name', 'school_id',
    ];

    public function school(){
        return $this->belongsTo(School::class, 'school_id');
    }
}
