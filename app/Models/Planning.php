<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Class_;
use App\Models\Planning_date;

class Planning extends Model
{
    protected $table = 'plannings';

    protected $fillable = [
        'id', 'name', 'class_id', 'nom_cours'
    ];

    public function class(){
        return $this->belongsTo(Class_::class, 'class_id');
    }

    public function planning_dates(){
        return $this->hasMany(Planning_date::class);
    }
}
