<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Planning;

class Planning_date extends Model
{
    protected $table = 'plannings_dates';

    protected $fillable = [
        'id', 'planning_id', 'start_date', 'end_date'
    ];

    public function plannings(){
        return $this->belongsTo(Planning::class, 'planning_id');
    }
}
