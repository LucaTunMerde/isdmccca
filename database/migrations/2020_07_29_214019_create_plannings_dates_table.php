<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanningsDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plannings_dates', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('planning_id')->unsigned()->default('0');
            $table->foreign('planning_id')->references('id')->on('plannings');
            $table->datetime('start_date')->default('0000-01-01 00:00:00');
            $table->datetime('end_date')->default('0000-01-01 23:59:00');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plannings_dates');
    }
}
